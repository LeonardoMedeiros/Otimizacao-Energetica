#include "asf.h"
#include "conf_board.h"
#include "conf_uart_serial.h"


/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

/**
 *  Informacoes para o RTC
 *  poderia ser extraida do __DATE__ e __TIME__
 *  ou ser atualizado pelo PC.
 */
#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        0
#define MINUTE      0
#define SECOND      0

/**
* LEDs
*/

#define USART_COM_ID ID_USART1
#define USART_COM    USART1


#define LED1_PIO_ID	   ID_PIOC
#define LED1_PIO        PIOC
#define LED1_PIN		31
#define LED1_PIN_MASK   (1<<LED1_PIN)

uint32_t hour, minute, second;

volatile int conter  = 0;
volatile uint8_t flag_led = 1; 


/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

//int Minute = MINUTE;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void BUT_init(void);
void LED_init(int estado);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/*
void USART1_Handler(void){
	uint32_t ret = usart_get_status(USART_COM);
	char c;

	if(ret & US_IER_RXRDY){
		usart_serial_getchar(USART_COM, &c);
	}
/**

*  Interrupt handler for TC1 interrupt.
*/
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrupo foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if (flag_led){
		pin_toggle(LED1_PIO, LED1_PIN_MASK);
		conter += 1;
	}
	
	if (conter == 10){
		conter = 0;
		tc_stop(TC0, 0);
		}
}

static void configure_console(void){
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
	#if (defined CONF_UART_CHAR_LENGTH)
			.charlength = CONF_UART_CHAR_LENGTH,
	#endif
			.paritytype = CONF_UART_PARITY,
	#if (defined CONF_UART_STOP_BITS)
			.stopbits = CONF_UART_STOP_BITS,
	#endif
		};

	stdio_serial_init(CONF_UART, &uart_serial_options);

#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	
#endif
}

static void USART1_init(void){
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4); // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;

	const sam_usart_opt_t usart_settings = {
		.baudrate       = 115200,
		.char_length    = US_MR_CHRL_8_BIT,
		.parity_type    = US_MR_PAR_NO,
		.stop_bits   	= US_MR_NBSTOP_1_BIT,
		.channel_mode   = US_MR_CHMODE_NORMAL
	};

	sysclk_enable_peripheral_clock(USART_COM_ID);

	usart_init_rs232(USART_COM, &usart_settings, sysclk_get_peripheral_hz());

	usart_enable_tx(USART_COM);
	usart_enable_rx(USART_COM);

	ptr_put = (int (*)(void volatile*,char))&usart_serial_putchar;
	ptr_get = (void (*)(void volatile*,char*))&usart_serial_getchar;

	//usart_enable_interrupt(USART_COM, US_IER_RXRDY);
    //NVIC_SetPriority(USART_COM_ID, 4);
	//NVIC_EnableIRQ(USART_COM_ID);

}

void RTC_init(){
	/* Configure o PMC */
	pmc_enable_periph_clk(ID_RTC);
	
	// Configure date and time
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);
	
	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);
	
}

void set_Alarm(){
	second += 3;
	
	if (second >= 60){
		minute += 1;
		minute = minute%60;
		second = second %  60;
	}
	
	if(minute >= 60){
		minute = minute % 60;
		hour += 1;
	}
	rtc_set_time_alarm(RTC, 1, hour, 1, minute, 1, second);
}

void RTC_Handler(void)

{
	uint32_t ul_status = rtc_get_status(RTC);
	
	//Verifica alarme
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		
		set_Alarm();
		flag_led = true;
		tc_start(TC0,0);
			
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}



/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
		pio_clear(pio, mask);
	else
		pio_set(pio,mask);
}



/**
* @Brief Inicializa o pino do LED
*/
void LED_init(int estado){
	
	pmc_enable_periph_clk(LED1_PIO_ID);
	pio_set_output(LED1_PIO, LED1_PIN_MASK, estado, 0, 0 );
};

/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrupco no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrupco no TC canal 0 */
	/* Interrupo no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}


/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	/* Configura Leds */
	LED_init(0);
	USART1_init();
	//RTC_init();

	/** Configura timer TC0, canal 1 */
	//TC_init(TC0, ID_TC0, 0, 32);

	/* configura alarme do RTC */
	//rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
	//rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE, 1, SECOND + 10);

	
	while (1) {
		uint32_t chars[200];
		
		/* Entrar em modo sleep */
		//pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
	

		usart_serial_getchar(USART_COM, &chars[0]);	
		
		if(chars[0] == '1'){
			pin_toggle(LED1_PIO, LED1_PIN_MASK);
			usart_serial_putchar(USART_COM, chars[0]);
		}
		
	}
}